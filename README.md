# Reference Electrode Converter

This software is a calculator that converts from a potential vs one reference electrode to another reference electrode. With this software you can convert between some aqueous reference electrodes (NHE, SCE, Ag/AgCl, etc.) and some non aqueous reference electrodes. This software is free to use and you are free to share it. Check the About to see details concerning this.

![The main window](img/main window.png "Main window")

## Getting started

It's very easy and intuitive to use this software.

![Aqueous view](img/aqueous.png "Aqueous view")

1. Just insert the value of potential to convert inside the box From, in milivolts.
2. Select the origin reference electrode corresponding to the value inserted in the box From.
3. Then, select the reference electrode you want convert to. 
4. Only for the Reversible Hydrogen Electrode (RHE) you must provide a value of pH.	
5. Press the button Calculate to make the conversion.	

In the Related equations section (6) you can see the involved equations used for the calculation. The first and the second are for the selected electrodes; the last equation is a relation of both selected electrodes. The Notes section (7) shows a note of the selected electrodes, if any note is present.

You can even see in a plot the values of every reference electrode used in this software. This is useful and intended for educational purposes.

![Plot of aqueous reference electrodes](img/plot_aq_ref_elec.png "Plot of aqueous reference electrodes") 

## Installation
No installation is needed. Just download the .exe file. Your antivirus may pop-up, just ensure to allow this aplication. Obviously you can run your antivirus on it to make sure it is safe to use.

## Contributing
You can contribute mainly by two ways:

1. Coding: if you are a programmer and you find out some bugs or can enhance the code of this software, proposals are always welcome.
2. Data: the data about Reference Electrodes are always welcome, especially for non aqueous reference electrodes.

## Authors and acknowledgment
Main core, data compilation, modules programming and GUI by Carlos Hernandez-Rodriguez.
Icons by Mark James (http://famfamfam.com).

## License
This software is licenced under GPLv3. See the Help and Licence for more information.
